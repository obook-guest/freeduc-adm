# freeduc-adm
FREEDUC-ADM
=============

L'image USB vive  [Freeduc-adm](https://admos.albertdemun.education/) est un projet d'après le dépot original de Georges Khaznadar
, réalisée depuis l'année 2019 à l'aide du paquet **live-build**
de Raphaël Hertzog et Daniel Baumann. Auparavant, le CDROM, puis
les images USB vives du projet [Freeduc](https://fr.wikipedia.org/wiki/Freeduc)
étaient basées sur la distribution vive [KNOPPIX](http://www.knopper.net/knoppix/)

Ce dépôt contient le travail réalisé pour l'enseignement des sciences et
et de la programmation à [l'ensemble scolaire Albert de Mun](https://www.albertdemun.fr)
à Nogent-sur-Marne.

Versions utilisables
--------------------

| Désignation |   Tag       |    Branche |   Commentaires |
| ----------- | ----------- | ---------- | ---------------------------------------------------------- |
| 2020.01     |    v2020.01 |   main | Version utilisable au lycée Albert de Mun à Nogent sur Marne pour l'enseignements des SNT |
