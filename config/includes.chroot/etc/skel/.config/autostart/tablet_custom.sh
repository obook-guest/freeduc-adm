#! /bin/sh

if aplay -L | grep -q bytcrrt5640; then
    # il y a une carte BYTCR-RT5640,
    # c'est probablement une tablette du lycée Jean Bart
    #
    # on configure ALSA
    cat > .asoundrc <<EOF
pcm.!default {
  type plug
  slave {
    pcm "hw:1,0"
  }
}
ctl.!default {
  type hw
  card 1
}
EOF
    # on configure la sortie speaker pour PULSEAUDIO
    pacmd set-sink-port \
      alsa_output.platform-bytcr_rt5640.HiFi__hw_bytcrrt5640__sink \
      "[Out] Speaker"
    # on arrête pulse
    pulseaudio --kill
    # on écrase les fichiers qui sont dans .config/pulse
    id=$(cat /etc/machine-id)
    for f in .config/pulse/default-jb-tablet/*; do
	g=$(echo $f | sed -e 's|default-jb-tablet/||' -e 's/machine-id/'${id}'/')
	cp $f $g
    done
    # on relance pulse, mais dans une minute
    echo "pulseaudio --start" | at now + 1 min
fi

# on désactive nm-applet vu que Wicd est installé
killall nm-applet
