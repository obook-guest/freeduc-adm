��    '      T  5   �      `     a     h  B   t     �     �     �     �  
   �     �  
               
   *     5     C     S     a     p     �     �     �     �     �  
   �     �     �  
   �     �                    *     9     E     Q     ^     k     w  O  �     �     �  O   �     C     ^     p     �  
   �     �  
   �     �     �  
   �     �     �     �               &     4     B     Q     `  
   n     y     �  
   �     �     �     �     �     �     �     �     �     	     	     	                	   !                $   #                                               
            "   %                                 '                                     &       Effect End of Drag Fade out the current window that the user is resizing or dragging. Opacify Windows Opacity Start of Drag Transition time easeInBack easeInBounce easeInCirc easeInCubic easeInElastic easeInExpo easeInOutBack easeInOutBounce easeInOutCirc easeInOutCubic easeInOutElastic easeInOutExpo easeInOutQuad easeInOutQuart easeInOutQuint easeInOutSine easeInQuad easeInQuart easeInQuint easeInSine easeOutBack easeOutBounce easeOutCirc easeOutCubic easeOutElastic easeOutExpo easeOutQuad easeOutQuart easeOutQuint easeOutSine milliseconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-12 15:22+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da
 Effekt Slutningen på trækket Gør vinduet, som flyttes eller hvis størrelse ændres, delvist gennemsigtigt. Gør vinduer gennemsigtige Uigennemsigtighed Starten på trækket Overgangstid easeInBack easeInBounce easeInCirc easeInCubic easeInElastic easeInExpo easeInOutBack easeInOutBounce easeInOutCirc easeInOutCubic easeInOutElastic easeInOutExpo easeInOutQuad easeInOutQuart easeInOutQuint easeInOutSine easeInQuad easeInQuart easeInQuint easeInSine easeOutBack easeOutBounce easeOutCirc easeOutCubic easeOutElastic easeOutExpo easeOutQuad easeOutQuart easeOutQuint easeOutSine millisekunder 